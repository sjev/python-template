# type: ignore

"""The setup script."""


import codecs
import os
import os.path

from setuptools import find_packages, setup


requirements = ["click"]

test_requirements = [
    "pytest>=3",
]

setup(
    author="{{ cookiecutter.author_name }}",
    author_email="{{ cookiecutter.author_email }}",
    python_requires=">=3.8",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.10",
    ],
    description="{{cookiecutter.project_short_description}}",
    install_requires=requirements,
    include_package_data=True,
    keywords="",
    name="{{ cookiecutter.package_name }}",
    package_dir={"": "src"},
    packages=find_packages("src"),
    test_suite="tests",
    tests_require=test_requirements,
    url="",
    # Use setuptools_scm for versioning
    use_scm_version={
        "write_to": "src/{{ cookiecutter.package_name }}/version.py",
        "fallback_version": "0.0.0+unknown",
    },
    # zip_safe=False,
    entry_points={
        "console_scripts": [
            "{{cookiecutter.package_name}}={{ cookiecutter.package_name }}.cli:cli"
        ]
    },
)
