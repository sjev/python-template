#!/usr/bin/env python3
"""
{{cookiecutter.package_name}} CLI
"""

import click
from {{cookiecutter.package_name}} import __version__


@click.group()
def cli():
    pass  # pragma: no cover


@cli.command()
def info():
    """ Print package info """
    print(__version__)


cli.add_command(info)

if __name__ == "__main__":
    cli()  # pragma: no cover
