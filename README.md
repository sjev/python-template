# ROX Python Package Cookiecutter

A [cookiecutter](https://cookiecutter.readthedocs.io/en/latest/README.html) (project template) for rapidly developing new open source Python packages. Best practices with all the modern bells and whistles included.

## Features

#### Automatic updates to the projects generated from this cookiecutter

* Powered by [cruft](https://cruft.github.io/cruft/)
* Keep your project up-to-date with best practices

#### Continuous integration

* Powered by Gitlab CI

#### Documentation

* Automatically published as gitlab pages
* Powered by [mkdocs-material](https://github.com/squidfunk/mkdocs-material)
* Auto-generated API documentation from docstrings via [mkdocstrings](https://github.com/mkdocstrings/mkdocstrings)
* See the extensive list of [MkDocs plugins](https://github.com/mkdocs/mkdocs/wiki/MkDocs-Plugins) which can help you
 to tune the documentation to fit your project's needs


#### Changelog management

* Gently enforced: [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
* GitHub releases get their description automatically populated based on the changelog content
* The _Unreleased_ section is automatically updated when a release is done
* Changelog is embedded in the documentation

#### Bells and whistles

* [pre-commit](https://pre-commit.com/) for running all the goodies listed below
* [mypy](https://flake8.pycqa.org/en/latest/) for static type checking
* pylint
* [black](https://black.readthedocs.io/en/stable/) for auto-formatting the code
* version control with git tags using `setuptools_scm`

## Usage

Make sure you have [`cruft`](https://github.com/cruft/cruft#installation) installed. Alternatively, you can use
 [`cookiecutter`](https://cookiecutter.readthedocs.io/en/latest/installation.html) if you are not interested in
  getting updates to the project "boilerplate" in the future.

Create a new project:

```sh
cruft create https://gitlab.com/sjev/python-template
```

The CLI interface will ask some basic questions, such the name of the project, and then generate all the goodies
 automatically.

After that you can make it a proper git repo:

```sh
cd <your-project-slug>
git init
git add .
git commit -m "Initial project structure from Wolt Python Package cookiecutter"
```

We update this cookiecutter template regularly to keep it up-to-date with the best practices of the Python world. You
 can get the updates into your project with:

```sh
cruft update
```
